<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class IndexController extends Controller
{
    public function generate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sectionsCount' => 'required|integer|between:3,5',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous())
                           ->withErrors($validator)
                           ->withInput($request->all());
        }

        return view('generate', ['sectionsCount' => $request->sectionsCount]);
    }
}
