@extends('template')
@section('title', 'Diplom Programm | Anastasiya')

@section('index.generate')
<div class="container">
    @for ($i = 1; $i <= $sectionsCount; $i++)
        <span class="badge badge-pill badge-info state-badge" >{{$i}}</span>
    @endfor
</div>
@stop
