@extends('template')
@section('title', 'Diplom Programm | Anastasiya')

@section('index')
<div class="alert alert-info">Please, enter the sections count to generate the group Machine</div>
<div class="offset-md-3 col-md-6">
    <form action="{{route('index.generate')}}" method="POST">
        {{ csrf_field() }}
        <div class="form-group col-md-8">
            <input class="form-control @if ($errors->has('sectionsCount')) is-invalid @endif" type="text" name="sectionsCount" value="{{old('sectionsCount')}}" placeholder="Sections Count"/>
            @if ($errors->has('sectionsCount'))
                <small class="form-text invalid-feedback">{{$errors->first('sectionsCount')}}</small>
            @endif
        </div>
        <div class="form-group col-md-4">
            <input class="btn btn-info" type="submit" value="Generate" />
        </div>
    </form>
</div>
@stop
